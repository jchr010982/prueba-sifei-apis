<?php
    class Producto{
        private $repositorio;
        public function __construct($repositorio) {
            $this->repositorio = $repositorio;
        }
        public function obtener_todos(){
            return  $this->repositorio->obtener_todos();
        }
    }
?>