<?php
    ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);

    header('Access-Control-Allow-Origin: *');
    

    include_once '../../infraestructura/producto.php';
    include_once '../../aplicacion/producto.php';
    
    $repositorio_producto = new RepositorioProducto();
    $aplicacion_producto = new Producto($repositorio_producto);
    if ($_SERVER['REQUEST_METHOD']=='GET')
    {
        $productos =$aplicacion_producto->obtener_todos();
        $respuesta = new stdClass();
        $respuesta->status= "success";
        $respuesta->data = $productos;
        $respuesta->code= null;
        $respuesta->message= null;
        echo json_encode($respuesta);
        exit();
    }
 
?>