<?php
    ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);
    
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET, POST');

    include_once '../../infraestructura/compra.php';
    include_once '../../aplicacion/compra.php';
    
    $repositorio_compra = new RepositorioCompra();
    $compra = new RepositorioCompra($repositorio_compra);

    if ($_SERVER['REQUEST_METHOD']=='POST')
    {
        echo($compra->guardar_compra(json_decode(file_get_contents('php://input'), true)));
        exit();
    }
 
?>