<?php
    class RepositorioProducto{
              
        private $servername;
        private $username;
        private $password;
        private $dbname;
        
        function __construct() {
            include("config.php");    
            $this->servername = $servername;
            $this->username = $username;
            $this->password = $password;
            $this->dbname = $dbname;
        }

        public function obtener_todos(){
            $result=null;
            try {
                $conn = new PDO("mysql:host=$this->servername;dbname=$this->dbname", $this->username, $this->password);
                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $stmt = $conn->prepare("SELECT * FROM Productos");
                $stmt->execute();
                $result =  $stmt->fetchAll(PDO::FETCH_ASSOC);
            } catch(PDOException $e) {
                echo "Error: " . $e->getMessage();
            }
            $conn = null;        
            return $result;
        }
    }
?>